import 'package:flutter/material.dart';
import 'package:golddyeapp/conf/const_values.dart';

Widget HomeScreentopElements(BuildContext context, _scaffoldState) {
  return Container(
    width: MediaQuery.of(context).size.width,
    padding: const EdgeInsets.all(20.0),
    decoration: const BoxDecoration(
        color: mainpageyellow,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        boxShadow: [BoxShadow(blurRadius: 20.0, color: mainpageyellow)]),
    child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        margin: const EdgeInsets.only(top: 30.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                _scaffoldState.currentState.openDrawer();
              },
              child: Container(
                child: const Icon(
                  Icons.menu,
                  color: Color(0xff56433f),
                ),
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Expanded(
                child: Text("${titlemain}",
                    style: const TextStyle(
                        color: Color(0xff56433f),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.7)))
          ],
        )),
  );
}
