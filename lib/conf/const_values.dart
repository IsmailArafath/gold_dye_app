import 'dart:ui';

import 'package:flutter/material.dart';

final String titlemain = "VELAYUTHAM GOLD DYE";
const Color maincolor = Color(0xffff9229);
const Color secColor = Color(0xffE7A03B);
const Color thcolor = Color(0xffF9E8D0);
const Color primaryTextColor = Color(0xFF414C6B);
const Color secondaryTextColor = Color(0xFFE4979E);
const Color titleTextColor = Colors.white;
const Color contentTextColor = Color(0xff868686);
const Color navigationColor = Color(0xFF6751B5);
const Color gradientStartColor = Color(0xFF0050AC);
const Color gradientEndColor = Color(0xFF9354B9);

const Color mainpageyellow = Color(0xffff9229);
