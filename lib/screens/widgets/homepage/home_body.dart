import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:golddyeapp/conf/const_values.dart';

Widget HomeScreenBodyElements(BuildContext context) {
  return Container(
    child: SafeArea(
        child: SingleChildScrollView(
      child: Column(
        children: [
          Container(
            child: Text("CHOOSE MODEL"),
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            height: 400.0,
            child: Swiper(
              itemCount: 15,
              itemWidth: MediaQuery.of(context).size.width - 100,
              itemHeight: MediaQuery.of(context).size.height - 400,
              layout: SwiperLayout.STACK,
              scale: 0.6,
              itemBuilder: (context, index) {
                return Container(
                  // padding: EdgeInsets.all(15.0),
                  child: Container(
                    // padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        color: Color(0xff7a7a7a),
                        borderRadius: BorderRadius.circular(30.0),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10.0,
                              offset: Offset(0, 14),
                              color: Color(0xff000000).withOpacity(0.2))
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(18.0),
                          child: Center(
                              child: Text("MODEL ${index}",
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: maincolor,
                                      fontWeight: FontWeight.bold))),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                            margin: const EdgeInsets.only(right: 20),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.all(15.0),
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(15),
                                    bottomRight: Radius.circular(15),
                                  ),
                                  color: Colors.white),
                              child: Container(
                                height: 150,
                                decoration: const BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            "https://assets.vogue.in/photos/5d80d8650757f0000877979b/1:1/w_700,h_700,c_limit/Gehna-Jewellers_Temple-Jewellery-3.jpg"))),
                              ),
                            )),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                            child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(left: 22.0),
                          padding: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                              color: maincolor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                bottomLeft: Radius.circular(15),
                              )),
                          child: Text(
                            'SELECT THIS',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w800),
                          ),
                        ))
                      ],
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    )),
  );
}
