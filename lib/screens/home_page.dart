import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:golddyeapp/conf/const_values.dart';
import 'package:golddyeapp/screens/widgets/homepage/home_body.dart';
import 'package:golddyeapp/screens/widgets/homepage/home_top.dart';

import 'main_drawer.dart';

class Homepage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomepageWidget();
  }
}

class HomepageWidget extends State<Homepage> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    // TODO: implement build
    return Scaffold(
        key: _scaffoldState,
        drawer: Drawer(
          child: Container(child: Drawerscreen()),
        ),
        backgroundColor: Color(0xfff2f2f2),
        body: Column(
          children: [
            HomeScreentopElements(context, _scaffoldState),
            HomeScreenBodyElements(context),
          ],
        ));
  }
}
