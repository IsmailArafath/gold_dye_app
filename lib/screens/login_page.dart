import 'package:flutter/material.dart';
import 'home_page.dart';
import '../conf/const_values.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen();
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenWidget();
  }
}

class LoginScreenWidget extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Container(color: maincolor),
          Container(
            margin: const EdgeInsets.only(top: 80),
            child: Column(
              key: null,
              children: [
                FittedBox(
                    key: null,
                    child: Text(titlemain,
                        style: const TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white))),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 150),
            padding: const EdgeInsets.all(25.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(50.0),
                    topRight: Radius.circular(50.0)),
                boxShadow: [
                  BoxShadow(
                      blurRadius: 40,
                      offset: const Offset(-5, -14),
                      color: Colors.black.withOpacity(0.2))
                ]),
            child: Container(
                child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Center(
                      child: FittedBox(
                        key: null,
                        child: Text(
                          "LOGIN TO YOUR ACCOUNT.",
                          style: TextStyle(fontSize: 18.0, color: maincolor),
                        ),
                      ),
                    ),
                    const SizedBox(height: 50.0),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10.0),
                              child: const FittedBox(
                                key: null,
                                child: Text("User Name",
                                    style: TextStyle(
                                        fontSize: 16.0, color: secColor)),
                              ),
                            ),
                            const SizedBox(
                              height: 5.0,
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              decoration: BoxDecoration(
                                  color: thcolor,
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(width: 1, color: secColor),
                                  boxShadow: [
                                    BoxShadow(
                                        blurRadius: 20.0,
                                        color: secColor.withOpacity(0.11),
                                        offset: const Offset(0, 10))
                                  ]),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                    hintText: "Enter Your user Name",
                                    border: InputBorder.none),
                              ),
                            ),
                            const SizedBox(
                              height: 15.0,
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10.0),
                              child: const FittedBox(
                                key: null,
                                child: Text("Password",
                                    style: TextStyle(
                                        fontSize: 16.0, color: secColor)),
                              ),
                            ),
                            const SizedBox(
                              height: 5.0,
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20.0),
                              decoration: BoxDecoration(
                                  color: thcolor,
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(width: 1, color: secColor),
                                  boxShadow: [
                                    BoxShadow(
                                        blurRadius: 20.0,
                                        color: secColor.withOpacity(0.11),
                                        offset: const Offset(0, 10))
                                  ]),
                              child: TextFormField(
                                decoration: const InputDecoration(
                                    hintText: "Enter Your Password",
                                    border: InputBorder.none),
                              ),
                            ),
                            const SizedBox(
                              height: 22.0,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 0.0),
                                child: RaisedButton(
                                  key: null,
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Homepage()));
                                  },
                                  elevation: 0,
                                  color: maincolor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30)),
                                  child: Container(
                                    padding: const EdgeInsets.all(15.0),
                                    child: const Text("LOGIN",
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w900)),
                                  ),
                                ))
                          ]),
                    )
                  ]),
            )),
          ),
        ],
      ),
    );
  }
}
