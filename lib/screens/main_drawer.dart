import 'package:flutter/material.dart';
import 'package:golddyeapp/conf/const_values.dart';

class Drawerscreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DrawerScreenWidget();
  }
}

class DrawerScreenWidget extends State<Drawerscreen> {
  @override
  var username = "";
  var loginuserid = "";
  setUsername() {
    setState(() {
      username = "Demo User";
      loginuserid = "demouser@gmail.com";
    });
  }

  void initState() {
    setUsername();
  }

  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        decoration: BoxDecoration(color: maincolor),
        child: Container(
          child: Column(
            children: [
              SizedBox(
                height: 80.0,
              ),
              Container(
                child: FittedBox(
                    child: Text("${titlemain}",
                        style: TextStyle(fontSize: 25.0, color: Colors.white))),
              ),
              // Container(
              //   width: double.infinity,
              //   height: 150.0,
              //   decoration: BoxDecoration(
              //       shape: BoxShape.circle,
              //       image: DecorationImage(
              //           image: AssetImage('assets/mainlogo.jpeg'))),
              // ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                width: double.infinity,
                child: SingleChildScrollView(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: FittedBox(
                          child: Text("${username}",
                              style: TextStyle(
                                  fontSize: 25.0, color: Colors.white))),
                    ),
                    Container(
                      child: FittedBox(
                          child: Text("${loginuserid}",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.white.withOpacity(0.8)))),
                    ),
                    SizedBox(
                      height: 120.0,
                    ),
                    InkWell(
                      onTap: () {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("coming soon"),
                        ));
                      },
                      child: Container(
                          width: double.infinity,
                          child: Column(
                            children: [
                              Container(
                                width: double.infinity,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Icon(Icons.person,
                                          color: Colors.white, size: 20.0),
                                    ),
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    Expanded(
                                        child: LimitedBox(
                                      child: Text("Profile",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.white)),
                                    )),
                                    Container(
                                      child: Icon(Icons.arrow_forward_ios,
                                          color: Colors.white, size: 20.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    InkWell(
                        onTap: () {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("coming soon"),
                          ));
                        },
                        child: Container(
                            width: double.infinity,
                            child: Column(
                              children: [
                                Container(
                                  width: double.infinity,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        child: Icon(Icons.call,
                                            color: Colors.white, size: 20.0),
                                      ),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Expanded(
                                          child: LimitedBox(
                                        child: Text("Contact Us",
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.white)),
                                      )),
                                      Container(
                                        child: Icon(Icons.arrow_forward_ios,
                                            color: Colors.white, size: 20.0),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ))),
                    SizedBox(
                      height: 20.0,
                    ),
                    InkWell(
                      onTap: () {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("coming soon"),
                        ));
                      },
                      child: Container(
                          width: double.infinity,
                          child: Column(
                            children: [
                              Container(
                                width: double.infinity,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Icon(Icons.email,
                                          color: Colors.white, size: 20.0),
                                    ),
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    Expanded(
                                        child: LimitedBox(
                                      child: Text("Chat",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.white)),
                                    )),
                                    Container(
                                      child: Icon(Icons.arrow_forward_ios,
                                          color: Colors.white, size: 20.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                    )
                  ],
                )),
              ),
            ],
          ),
        ));
  }
}
